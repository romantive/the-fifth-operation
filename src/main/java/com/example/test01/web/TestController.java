package com.example.test01.web;

import com.example.test01.dao.database1.StudentDaoRepository;
import com.example.test01.dao.database2.DormitoryDaoRepository;
import com.example.test01.entity.database1.Student;

import com.example.test01.entity.database2.Dormitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TestController {

    @Autowired
    private StudentDaoRepository userDaoRepository;

    @Autowired
    private DormitoryDaoRepository studentDaoRepository;

    @GetMapping(value = "/getStudent")
    public List<Student> getuser(){
        List<Student> all = userDaoRepository.findAll();
        return all;
    }
    @GetMapping(value = "/getDormitory")
    public List<Dormitory> getstudent(){
        List<Dormitory> all = studentDaoRepository.findAll();
        return all;
    }
}
