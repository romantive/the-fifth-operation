package com.example.test01.entity.database2;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "dormitory")
@Data
public class Dormitory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer num;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return "Dormitory{" +
                "id=" + id +
                ", num=" + num +
                '}';
    }
}
