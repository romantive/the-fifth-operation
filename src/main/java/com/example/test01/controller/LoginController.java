package com.example.test01.controller;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.test01.entity.database1.Student;
import com.example.test01.dao.database1.StudentDaoRepository;
import com.example.test01.dao.database2.DormitoryDaoRepository;
import com.example.test01.entity.database1.Student;
import com.example.test01.entity.database2.Dormitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

@RestController
public class LoginController {
    @Autowired
    private StudentDaoRepository studentDaoRepository;

    @Autowired
    private DormitoryDaoRepository dormitoryDaoRepository;

    @PostMapping("/Student/login")   //登陆
    public String findByName(HttpServletRequest request,HttpServletResponse response,@RequestParam("name") String name, @RequestParam("password") String password) {

        String md5Password = DigestUtils.md5DigestAsHex(password.getBytes());
        Student stu = studentDaoRepository.findByNameAndPassword(name,md5Password);
        if(stu==null) {
            return "用户名或密码错误，登陆失败";
        }
        Map<String,String> payload =new HashMap<>();
        payload.put("name",stu.getName());
        payload.put("sex",stu.getSex());
        //String token = JWTUtils.getToken(payload);    //修改部分   增加了方法传递的参数HttpServletResponse response
        HttpSession session = request.getSession();
        String sessionId = session.getId();
        session.setAttribute("username",name);
        Cookie cookie = new Cookie("sessionId", sessionId);    //修改部分
        cookie.setPath(request.getContextPath());
        response.addCookie(cookie);         //修改部分
        return stu.getName()+"你好，你已登陆成功\nsessionId为："+sessionId;
    }

    @GetMapping("/Student/list")
    public Map<String,Object> getInTo(HttpServletRequest request,HttpServletResponse response) throws IOException {
        Cookie[] cookies = request.getCookies();
        if (null == cookies) {
            response.sendRedirect(request.getContextPath() + "/Student/login");
            Map<String,Object> map = new HashMap<>();
            map.put("state",false);
            map.put("msg","cookie为空");
            return map;
        }
        //String token = null;
        String sessionId = null;
        for (Cookie item : cookies) {
            if ("sessionId".equals(item.getName())) {
                sessionId = item.getValue();
                break;
            }
        }
        if(sessionId==null){
            Map<String,Object> map = new HashMap<>();
            map.put("state",false);
            map.put("msg","cookie中没有sessionId");
            return map;
        }
        HttpSession session = request.getSession();
        if(!session.getId().equals(sessionId)){
            Map<String,Object> map = new HashMap<>();
            map.put("state",false);
            map.put("msg","sessionId错误");
            return map;
        }
        Map<String,Object> map = new LinkedHashMap<>();
        //处理业务逻辑
        //DecodedJWT verify = JWTUtils.verify(token);
        //String name = verify.getClaim("name").asString();
        Object name = session.getAttribute("username");
        map.put("state",true);
        map.put("msg",name+"你好,你已登陆成功");
        List<Dormitory> all = dormitoryDaoRepository.findAll();
        map.put("info",all);
//        for(int i=1;i<=3;i++){
//            int num = studentDaoRepository.countByDomid(i);
//            map.put("dormitory"+Integer.toString(i),Integer.toString(num));
//        }
        return map;
    }

    @PostMapping("/Student/register")      //学生信息注册
    public String register(@RequestParam("name") String name, @RequestParam("sex") String sex, @RequestParam("password") String password,@RequestParam("domid") Integer domid) {
        Student stu = studentDaoRepository.findByName(name);
        if(stu!=null){
            return "该用户名已注册";
        }
        if(!Objects.equals(sex, "男") && !Objects.equals(sex, "女")) {
            return "注册失败：请输入正确的性别：男/女";
        }
        for(int i=0;i<password.length();i++){
            char a=password.charAt(i);
            if(!((a>='0'&&a<='9')||(a>='a'&&a<='z')||(a>='A'&&a<='Z'))){
                return "注册失败：密码不能包含特殊字符";
            }
        }
        if(password.length()<=5) {
            return "注册失败：请输入六位以上的数字或字母作为密码";
        }
        String md5Password = DigestUtils.md5DigestAsHex(password.getBytes());
        Student student = new Student();
        student.setName(name);
        student.setSex(sex);
        student.setPassword(md5Password);
        student.setDomid(domid);
        studentDaoRepository.save(student);
        return "注册成功";
    }

}
