package com.example.test01.dao.database1;

import com.example.test01.entity.database1.Student;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import javax.persistence.criteria.CriteriaBuilder;

public interface StudentDaoRepository extends JpaRepository<Student, Integer>, JpaSpecificationExecutor<Student> {
    Student findByName(String name);
    Student findByNameAndPassword(String name,String password);
    int countByDomid(Integer domid);
}
