package com.example.test01.dao.database2;

import com.example.test01.entity.database2.Dormitory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DormitoryDaoRepository extends JpaRepository<Dormitory, Integer>, JpaSpecificationExecutor<Dormitory> {
}
